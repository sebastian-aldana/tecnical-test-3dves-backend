# 3DVES Backend - test

Prueba tecnica desarrollador 3DVES.

## [Consulta este backend en now.sh](https://tecnical-test-3dves-csxp4jntj.now.sh/)

### iniciar el proyecto

- clonar el repositorio:
  ```bash
  $ git clone git@gitlab.com:sebastian-aldana/tecnical-test-3dves-backend.git
  ```
- instalar dependencias
  ```bash
  $ cd tecnical-test-3dves-backend
  $ npm install
  ```
  
- iniciar el servidor de desarrollo

  ```bash
  $ npm star

  ```
  
- este backend los podemos consultar en localhost:3000
