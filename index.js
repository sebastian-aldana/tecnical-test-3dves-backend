const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const CORS = require("cors");

app.use(CORS({ origin: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var contacts = {};

app.get("/contacts", (req, res) => {
  res.send(Object.values(contacts));
});

app.get("/contacts/update/:id", (req, res) => {
  res.send(contacts[req.params.id]);
});

app.post("/contacts/create", (req, res) => {
  const { name, lastName, phone } = req.body;
  const index = Object.keys(contacts).length;
  contacts[index] = { id: index, name, lastName, phone };
  res.send({
    info: "contact created"
  });
});

app.post("/contacts/update", (req, res) => {
  const { name, lastName, phone, id } = req.body;
  contacts[req.body.id] = { id, name, lastName, phone };
  res.send({
    info: "contact updated"
  });
});

app.delete("/contacts/delete", (req, res) => {
  delete contacts[req.body.id];
  res.send({
    info: "contact deleted"
  });
});

app.listen(3000, err => {
  if (err) {
    console.error("Error: ", err);
    return;
  }
  console.log(`Listening http://localhost:${3000}`);
});
